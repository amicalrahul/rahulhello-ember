module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-coffeelint');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-ember-templates');
    grunt.loadNpmTasks('grunt-emblem');

    grunt.registerTask('default', [
        'coffeelint',
        'coffee',
        'emberTemplates',
        'emblem',
        'connect',
        'watch'
    ]);

    grunt.registerTask('serve', ['connect']);

    grunt.initConfig({
        coffee: {
            compile: {
                files: {
                    'build/app.js': [ 'app/coffee/**/*.coffee' ]
                },
                options: {
                    bare: true
                }
            }
        },

        coffeelint: {
            all: ['app/coffee/**/*.coffee'],
            options: {
                configFile: 'coffeelint.json'
            },
        },

        connect: {
            server: {
                options: {
                    port: 8000,
                    livereload: true,
                    base: ['build', 'libs', '.']
                }
            }
        },

        emberTemplates: {
            compile: {
                options: {
                    precompile: false,
                    templateBasePath: 'app/templates'
                },
                files: {
                    'build/templates.js': 'app/templates/**/*.hbs'
                }
            }
        },

         emblem: {
             compile: {
                 files: {
                     'build/templates-emblem.js': ['app/templates/**/*.emblem']
                 },
                 options: {
                     root: 'app/templates/',
                     dependencies: {
                         handlebars: 'libs/handlebars-1.1.2.js',
                         jquery: 'libs/jquery-1.10.2.js',
                         ember: 'libs/ember-1.6.1.js',
                         emblem: 'libs/emblem.js',
                     }
                 }
             }
         },
        watch: {
            coffee: {
                files: ['app/coffee/**/*.coffee'],
                tasks: ['coffeelint', 'coffee']
            },
            html: {
                files: ['index.html'],
                tasks: []
            },
            handlebars: {
                files: ['app/templates/**/*.hbs'],
                tasks: ['emberTemplates']
            },
            emblem: {
                files: ['app/templates/**/*.emblem'],
                tasks: ['emblem']
            },
            options: {
                livereload: true
            }
        },
    });
};
